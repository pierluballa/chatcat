module.exports = function(express, app, passport, config, rooms){
	var router = express.Router();

	function securePages(req,res,next){
		if (req.isAuthenticated()){
			next();	
		}else{
			res.redirect('/');
		}
	}
	//FACEBOOK
	router.get('/auth/facebook',passport.authenticate('facebook'));
	router.get('/auth/facebook/callback',passport.authenticate('facebook',{
		successRedirect: '/chatrooms',
		failureRedirect: '/'
	}));
	router.get('/logout', function(req,res,next){
		req.logout();
		res.redirect('/');
	});

	router.get('/', function(req,res,next){
		res.render('index', {title: 'Welcome'});
	});

	router.get('/chatrooms', securePages, function(req,res,next){
		res.render('chatrooms', {
			title: 'Chatrooms',
			user: req.user,
			config: config
		});
	});

	router.get('/room/:id', securePages, function(req,res,next){
		var room_name = findTitle(req.params.id);
		res.render('room', {
			user: req.user,
			room_number: req.params.id,
			config: config,
			room_name: room_name

		});	
		
	});

	function findTitle(room_id){
		var n = 0;
		while(n< rooms.length){
			if (rooms[n].room_number == room_id){
				return 	rooms[n].room_name;
				break;	
			} else {
				n++;
				continue;
			}
		}
	}

	//Demostrate session
	router.get('/setcolor', function(req,res,next){
		req.session.favColor = 'Red';
		res.send('Setting favorite color: '+req.session.favColor);
		
	});

	router.get('/getcolor', function(req,res,next){
		res.send(req.session.favColor);
	});

	app.use('/', router);
};