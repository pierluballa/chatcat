module.exports = function(io, rooms){
	var chatrooms = io.of('/roomlist').on('connection', function(socket){ // Namespace corrispondente nel client
		console.log('Connection "roomlist" Established on Server!');

		socket.emit('roomupdate', JSON.stringify(rooms));

		socket.on('newroom', function(data){
			rooms.push(data);
			socket.broadcast.emit('roomupdate', JSON.stringify(rooms)); // to all user except chi emette l'evento
			socket.emit('roomupdate', JSON.stringify(rooms)); 
		});
	});

	var messages = io.of('/messages').on('connection', function(socket){ // Namespace corrispondente nel client
		console.log('Connection "messages" Established on Server!');

		socket.on('joinroom', function(data){
			console.log("joinroom: "+ JSON.stringify(data));
			socket.username = data.user;
			socket.userPic = data.userPic;
			socket.join(data.room);
			updateUserList(data.room, true);
		});

		socket.on('newMessage', function(data){
			console.log("newMessage: "+ JSON.stringify(data));
			
			socket.broadcast.to(data.room_number).emit('messagefeed', JSON.stringify({
				userPic: data.userPic,
				message: data.message
			}));
		});


		function updateUserList(room, updateAll){
				var nsp = io.of('/messages');
				var roomSocket = nsp.adapter.rooms[room];
				var userList = [];
				for (var socketId in roomSocket) {
    				//console.log(socketId);
    				var client = nsp.connected[socketId];

    				userList.push({
						user: client.username,
						userPic: client.userPic,
					});
    			}
    			//console.log(userList);
    			socket.to(room).emit('updateUserList', JSON.stringify(userList));
				if (updateAll){
					socket.broadcast.to(room).emit('updateUserList', JSON.stringify(userList));
				};
				//var getUsers = io.of('/messages').adapter.rooms[room];
				//console.log("getUsers" + JSON.stringify(getUsers));
				//console.log(io.sockets.connected[getUsers[0]]);
				/*var userList = [];
				for (var clientId in getUsers ) {
					var clientSocket = io.of('/messages').connected[clientId];
					userList.push({
						user: clientSocket.username,
						userPic: clientSocket.userPic,
					});
				};
				socket.to(room).emit('updateUserList', JSON.stringify(userList));

				if (updateAll){
					socket.broadcast.to(room).emit('updateUserList', JSON.stringify(userList));
				};*/
		}

		socket.on('updateList', function(data){
				updateUserList(data.room);
		});


	});
}