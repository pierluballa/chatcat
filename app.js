var express = require('express'),
	app = express(),
	path = require('path'),
	cookieParser = require('cookie-parser'),
	session = require('express-session'),
	connectMongo = require('connect-mongo')(session),
	passport = require('passport'),
	FacebookStrategy = require('passport-facebook').Strategy;

var rooms = [];	

var env = process.env.NODE_ENV || 'production';
var config = require('./config/config.js');
var mongoose = require('mongoose').connect(config.dbURL);

app.set('views', path.join(__dirname, 'views'));
app.engine('html', require('hogan-express'));
app.set('view engine', 'html');

app.use(express.static(path.join(__dirname, 'public')));

app.use(cookieParser());

if (env !== 'production'){
	app.use(session({secret: config.sessionSecret, saveUninitialized: true, resave: true}));
}
else{
	app.use(session({
		secret: config.sessionSecret, 
		saveUninitialized: true, 
		resave: true,
		store: new connectMongo({
			//url: config.dbURL,
			mongooseConnection: mongoose.connection,
			stringify: true
		})
	}));
}

//TEST DB
/*var userSchema = mongoose.Schema({
	username: String,
	password: String,
	fullname: String,
});
var Person = mongoose.model('users', userSchema);
var John = new Person({
	username: "John",
	password: "Doe",
	fullname: "John Doe",
});
John.save(function(err){console.log('SAVED test to db')});*/

app.use(passport.initialize());
app.use(passport.session());
require('./auth/passportAuth.js')(passport, FacebookStrategy, config, mongoose);
require('./routes/routes.js')(express,app,passport,config,rooms);

/*app.listen(3000, function(){
	console.log("Start!!!");
});*/
app.set('port', process.env.PORT || 3001);
var server = require('http').createServer(app);
var io = require('socket.io').listen(server);
require('./socket/socket.js')(io, rooms);
server.listen(app.get('port'), function(){
	console.log('ChatCat running on port: '+ app.get('port'));
});